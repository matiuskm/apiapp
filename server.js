var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;

var CONTACTS_COLLECTION = "contacts";

var app = express();
app.use(bodyParser.json());

var db;
var mongoUri = 'mongodb://admin:admin@ds141434.mlab.com:41434/testapi';
var router = express.Router();


// CONTACTS API ROUTES BELOW

//Generates error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

router.use(function(req, res, next) {
  console.log("something is happening");
  next();
});

router.route('/contacts')
      .get(function(req, res) {
        db.collection(CONTACTS_COLLECTION).find({}).toArray(function(err, docs) {
          if (err) {
            handleError(res, err.message, "Failed to get contacts.");
          } else {
            if (docs.count > 1) {
              res.status(200).json(docs);
            } else {
              res.status(200).json({message: 'No contacts in your collection'});              
            }
          }
        });
      })
      .post(function(req, res) {
        var newContact = req.body;

        if (!req.body.name) {
          handleError(res, "Invalid user input.", "Must provide a name.", 400);
        }

        db.collection(CONTACTS_COLLECTION).insertOne(newContact, function(err, doc) {
          if (err) {
            handleError(res, err.message, "Failed to create a new contact.");
          } else {
            res.status(201).json(doc.ops[0]);
          }
        });
      });

router.route('/contacts/:id')
      .get(function(req, res) {
      })
      .put(function(req, res) {
      })
      .delete(function(req, res) {
      });

router.get('/', function (req, res) {
  res.status(200).json({message: 'Contacts API v.1!!'})
});

mongodb.MongoClient.connect(mongoUri, function (err, database) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  db = database;
  console.log("Database connection ready!");

  app.use('/api', router);
  var server = app.listen(process.env.PORT || 8080, function() {
    var port = server.address().port;
    console.log("App now running on port ", port);
  });
});
